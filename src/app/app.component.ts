import { Component, OnInit } from "@angular/core";
import { StateService } from "./state.service";
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { WebSocketService } from './WebSocketService'

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  socket: Subject<any>;
  message: string;
  sentMessage: string;
  title = "angular-challenge";
  sidenavOpened$ = this.state.sideNavState$;
  constructor(private state: StateService, private router: Router, private WebSocketService:WebSocketService) 
  {
    this.socket = WebSocketService.createWebsocket();
  }
  ngOnInit()
  {
    this.socket.next({   "event": "measurements",
    "data": "test"});
    this.socket.subscribe(message => console.log(message) );
  }
  openSidebar(): void {
    this.sidenavOpened$.next(true);
  }

  closeSideBar() {
    this.sidenavOpened$.next(false)
  }
  navigate(route:string)
  {
    this.router.navigate([route]);
  }
}
