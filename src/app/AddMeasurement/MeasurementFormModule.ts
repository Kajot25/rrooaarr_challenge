import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeasurementFormRoutingModule } from './MeasurementForm-routing-module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AddMeasurementComponent } from './AddMeasurementForm/add-measurement.component';

@NgModule({
  imports: [
    CommonModule,
    MeasurementFormRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [AddMeasurementComponent]
})
export class MeasurementFormModule { }