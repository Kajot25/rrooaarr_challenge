import { Component, OnInit } from '@angular/core';
import { Measurement } from 'src/models/measurement.interface';
import { mockdata } from "../../mockdata";


@Component({
  selector: 'app-add-measurement',
  templateUrl: './add-measurement.component.html',
  styleUrls: ['./add-measurement.component.scss', "../../../../node_modules/bootstrap/dist/css/bootstrap.min.css",]
})
export class AddMeasurementComponent implements OnInit {
  Measurement: Measurement = {id:"",name:"",value:0,scientist:"",time:Date()};
  constructor() 
  {  }

  ngOnInit(): void {
  }
  onAdd(f)
  {
    console.log(this.Measurement);
    
    mockdata.push(this.Measurement);
    console.log(mockdata);
    
  }
}
