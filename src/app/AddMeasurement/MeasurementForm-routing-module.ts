import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddMeasurementComponent} from "./AddMeasurementForm/add-measurement.component";
import {DatatableModule} from "../datatable/datatable.module";

const routes: Routes = [
  {
    path: '',
    component: AddMeasurementComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), DatatableModule],
  exports: [RouterModule]
})
export class MeasurementFormRoutingModule { }