import { Injectable } from '@angular/core';
import { Subject, Observer, Observable } from 'rxjs';
@Injectable()
export class WebSocketService{
  public createWebsocket(): Subject<MessageEvent> {
        let socket = new WebSocket('wss://challenge3.rrooaarr.cloud/');
        let observable = Observable.create(
                    (observer: Observer<MessageEvent>) => {
                        socket.onmessage = observer.next.bind(observer);
                        socket.onerror = observer.error.bind(observer);
                        socket.onclose = observer.complete.bind(observer);
                        return socket.close.bind(socket);
                    }
        );
        let observer = {
                next: (data: Object) => {
                    if (socket.readyState === WebSocket.OPEN) {
                        socket.send(JSON.stringify(data));
                    }
                }
        };
        return Subject.create(observer, observable);
  }
}